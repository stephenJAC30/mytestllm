import os

from openai import OpenAI
client = OpenAI()

# openai_api_key = os.environ.get("OPENAI_API_KEY")

prompt_system = ("You are an experienced Python programmer. Please generate the code based on the user provided "
                 "examples and the last comment.")


prompt_mir_lmp_controller_ui = """
# Python MiR Autonomous Mobile Robot control script
import numpy as np
from env_utils import go_to_position dock_to_position play_sound show_light run_program
from plan_utils import parse_position

# Let robot MiR UR5 go to position UR Home 1.
say('OK - Requesting MiR UR5 to move to UR Home 1.')
go_to_position('MiR UR5, 'UR Home 1')

# UR5, go home
say('OK - Requesting MiR UR5 to move to its home position.')
go_to_position('MiR UR5, 'UR Home 1')

# 'UR5, show red light
say('OK - Requesting MiR UR5 to flash Red light.')
show_light('MiR UR5', 'Red')

# UR5. flash red light 10 times.
say('OK - Requesting MiR UR5 to flash Red light 10 times.')
for _ in range(10):
    show_light('MiR UR5', 'Red')

# Roller, go to room U205.
"""


completion = client.chat.completions.create(
  model="gpt-3.5-turbo",
  messages=[
    {"role": "system", "content": prompt_system},
    {"role": "user", "content": prompt_mir_lmp_controller_ui}
  ]
)
print (completion.choices[0].message.content.strip())
