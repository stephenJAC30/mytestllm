

conda create -n llm python==3.9 numpy openai
conda activate llm
conda install pygments pyaudio -y
conda install pydub --channel conda-forge -y
conda install pygame --channel conda-forge -y
conda install astunparse --channel conda-forge -y
conda install requests -y


python mir_lmp_controller_ui.py text


